class Controller_BOauto:

    def __init__(self, model, view):
        self.model = model
        self.view = view

    def screen_manager(self, screen):
        if "initializer" in screen:
            self.view.initializer()

        elif "in_learning" in screen:
            self.view.in_learning()

    def request_syntax_and_operation_check(self, custom_eval, output_size, output_dimension=1):
        try:
            syntax_and_operation_check_result = \
                self.model.check_syntax_and_operation(custom_eval, output_size, output_dimension)
        except Exception:
            print("Error raised, rewrite evaluation function")
        else:
            return float(syntax_and_operation_check_result)

    def request_save_init_config(self, init_config):
        pass

