
import optuna
from abc import ABCMeta, abstractmethod, abstractstaticmethod, abstractproperty
import numpy as np


class Calculator:

    __evaluation_method = ("Max", "Min", "Arithmetic mean", "Geometric mean", "Harmonic mean", "Trimmed mean",
                           "Standard deviation", "CV", "Median", "Mode", "Custom")

    def __init__(self):
        pass

    @property
    def available_evaluation_method(self) -> tuple:
        return self.__evaluation_method

    def evaluation_calculate(self) -> float:
        pass

    def arithmetic_mean(self) -> float:
        pass

    def geometric_mean(self) -> float:
        pass

    def harmonic_mean(self) -> float:
        pass

    def trimmed_mean(self) -> float:
        pass

    def max(self) -> float:
        pass

    def min(self) -> float:
        pass

    def std(self) -> float:
        pass

    def median(self) -> float:
        pass

    def mode(self) -> float:
        pass

    def custom(self):
        pass

