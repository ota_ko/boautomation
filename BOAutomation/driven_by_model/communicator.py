
import pymcprotocol
from abc import ABCMeta, abstractmethod, abstractstaticmethod, abstractproperty


class Communicator:

    __available_communication_ways = ("MC Protocol 3E(via PLC)", "MC Protocol 4E(via PLC)", "Manual")
    __plcs = ("Q(KEYENCE KV series)", "L", "QnA", "iQ-R", "iQ-L")

    def __init__(self):
        pass

    @property
    def available_communication_ways(self):
        return self.__available_communication_ways

    @property
    def available_plcs(self):
        return self.__plcs

    def send_to_plc(self):
        pass

    def receive_from_plc(self):
        pass

    def test_communication(self):
        pass
