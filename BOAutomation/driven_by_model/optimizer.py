
import optuna
from abc import ABCMeta, abstractmethod, abstractstaticmethod, abstractproperty
import numpy as np


class Optimizer:

    def __init__(self):
        self.study = None

    def create(self):
        # 変数の種類によって trial.suggest_categorical/trial.suggest_int/trial.suggest_uniformを使い分ける
        # またユーザによって探索範囲のスケールが指数ベースで表されるような広さの場合、trial.trial.suggest_loguniformを使用
        # log_uniformに関する参考：https://qiita.com/kenchin110100/items/ac3edb480d789481f134
        # ユーザーが入力したパラメータファイルを参考にしながら、↑を行う

        self.study = optuna.create_study()

    # 探索するべき関数
    def objective(self):
        # 通信手段 = マニュアル もしくは Change_parameter_physically = Yesの場合
        """



        :return 0
        """

    # 学習状況をmodel,controllerを通してviewへフィードバックする
    def update(self):

        pass

    # パラメータ探索終了時に学習結果を表示するためのメソッド
    def result(self):

        pass

    # 探索を中断する際のTrials保存処理
    def pause(self):
        pass

    # 中断した探索を再開する際のメソッド
    def resume(self):
        pass

