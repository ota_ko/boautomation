from driven_by_model import optimizer, communicator, calculator
import subprocess


class Model_BOauto:

    def __init__(self):
        self.optimizer = optimizer.Optimizer()
        self.communicator = communicator.Communicator()
        self.calculator = calculator.Calculator()
        self.syntax = None

    # ToDo:多目的最適化を解くにあたって次元数を拡張したい
    def check_syntax_and_operation(self, custom_eval, output_size, output_dimension=1):
        file_path = "./driven_by_model/check_syntax_and_operation.py"
        syntax = "import numpy as np\n" + \
                 "out = np.ones({},)\n".format(output_size) + \
                 "value = {}\n".format(custom_eval) + \
                 "print(value)"
        with open(file_path, mode='w') as f:
            f.write(syntax)

        proc = subprocess.run("python {}".format(file_path),
                              encoding='utf-8', stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        if proc.stderr != "":
            raise Exception
        else:
            return proc.stdout

    def save_init_config(self, init_config):
        pass