"""
**kwargsの使い方
"""


def hello_judgement(hello, **kwargs):
    # キーの全定義をハードコーディング
    available_keys = ['time']

    # ない場合はデフォルトの値を入れておく
    kwargs = key_check(available_keys, list(kwargs.keys()), kwargs)
    if hello == "say":
        if kwargs['time'] == 'night':
            print("na-na, It's good-night time, dude.")
        elif kwargs['time'] == 'noon':
            print("hello there!")
        else:
            print("idk whether it's hello-time or not")
    else:
        print("zzz")


# デフォルト値としてNoneを代入する関数
def key_check(available_keys, args, kwargs):
    for _ in available_keys:
        if _ not in args:
            kwargs[_] = None
    if kwargs == {}:
        for _ in available_keys:
            kwargs[_] = None
    return kwargs


hello_judgement(hello="say", time='night')
hello_judgement(hello="say", time='noon')
hello_judgement(hello="", time='night')
hello_judgement(hello="say")
