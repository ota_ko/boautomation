
"""
st.session_stateで値を保持する方法
参照：https://docs.streamlit.io/en/stable/session_state_api.html
streamlitではrerun毎に値がリセットされてしまうためこのような処置が必要
"""

import streamlit as st


class session:
    def __init__(self):
        if 'abc' not in st.session_state:
            st.session_state['abc'] = 0
        self.callback = callback()

    def screen(self):
        st.session_state["abc"] = st.number_input("input:", on_change=self.callback.callback, kwargs={'device': st.session_state["abc"]})


class callback:
    def __init__(self):
        pass

    def callback(self, device):
        if device == 0:
            st.write(device)
            st.write("0です")
        else:
            st.write("0じゃないです")

if __name__ == "__main__":
    session = session()
    session.screen()
