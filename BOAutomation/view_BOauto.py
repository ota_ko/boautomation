# MVC参照：https://nekogata.hatenablog.com/entry/2013/11/11/075234

import streamlit as st
import pandas as pd
import numpy as np
import inspect



class View_BOauto:
    # 値はすべてControllerへの送信用
    def __init__(self, model):
        self.model = model
        self.ui = None
        self.setting_value = ["communicating_way", "output_size", "evaluation_method", "custom_evaluation",
                              "evaluation_dimension", "plc", "initial_device", "iteration_numer"]
        self.init_config = {}
        for _ in self.setting_value:
            if _ not in st.session_state:
                st.session_state[_] = None

        self.min_output_size = 1
        self.max_output_size = 100
        # ToDo:機種ごとに分けたい(dict型を使用したい)。この場合initではなくinitialzerメソッドへ移動
        self.min_device = 0
        self.max_device = 100000

        self.min_iteration_num = 0
        self.max_iteration_num = 1000

    def ui_instance_set(self, ui):
        self.ui = ui



    def initializer(self):
        st.header("設定を始めましょう！")

        # PLCとの通信方法
        st.subheader("通信するPLCを選択してください")
        st.session_state["communicating_way"] = st.selectbox("", self.model.communicator.available_communication_ways)

        # 入力の設定
        st.subheader("フォームをダウンロードした後、生産パラメータのメタデータを入力し、アップロードしてください")
        st.download_button("download template form", data="template_input_values.xlsx",
                           mime="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")

        # 出力データのサイズ
        st.subheader("出力データのサイズを入力してください")
        st.session_state["output_size"] = st.number_input("", min_value=self.min_output_size, max_value=self.max_output_size)

        # PLCとの通信を行う場合の設定(PLCの種類、デバイス番号)
        if not st.session_state["communicating_way"] == "Manual":
            st.subheader("PLCの種類を選択して下さい")
            st.session_state["plc"] = st.selectbox("", self.model.communicator.available_plcs)
            # ToDo:設定できる最大のデバイス番号と機種をリンクさせる(dict型? key:機種、value:デバイス番号)
            st.subheader("データ送受信するPLCの先頭デバイスを入力してください")
            st.session_state["initial_device"] = \
                "D{}".format(st.number_input("", min_value=self.min_device, max_value=self.max_device))

        # 出力データの評価方法
        st.subheader("出力データの評価方法を選択してください")
        st.session_state["evaluation_method"] = st.selectbox("", self.model.calculator.available_evaluation_method)

        # Manual時、評価関数の作成
        if st.session_state["evaluation_method"] == "Custom":
            # ToDo: カスタム評価関数は関数をフックさせる形式に変更予定
            st.session_state["output_size"] = st.slider("", max=self.min_output_size, min=self.max_output_size,
                                                        step=self.min_output_size)
            """
            st.session_state["output_dimension"] = st.slider("", max=self.min_dimension_size, min=self.max_dimension_size,
                                                        step=self.min_dimension_size)
            """
            st.session_state["custom_evaluation"] = \
                st.text_input("", on_change=self.ui.callback_syn_and_opr_check(st.session_state["custom_evaluation"]))
            if st.checkbox("評価式を書きましたか？"):
                try:
                    syntax_and_operation_check_result = self.ui.WebUI.callback_syn_and_opr_check(st.session_state["custom_evaluation"])
                except Exception:
                    st.write("エラーが発生しました。チェックを外して再入力してください。")
                else:
                    if st.checkbox("全出力が1の場合の計算結果は:{} ですか?".format(syntax_and_operation_check_result)):
                        st.write("評価式を登録しました")

        # 探索回数
        st.subheader("探索回数を入力してください")
        st.session_state["iteration_number"] = st.number_input("", min_value=self.min_iteration_num, max_value=self.max_iteration_num)


        # 初期設定とりまとめ→保存依頼
        st.subheader("設定内容は以下の通りです")
        for _ in self.setting_value:
            self.init_config[_] = st.session_state[_]
            st.write(_, "is", self.init_config[_])

        if st.checkbox("最適化を開始しますか？"):
            self.ui.callback_save_init_config(self.init_config)

    def in_learning(self):
        st.write("in_learning")

        for _ in self.setting_value:
            st.write(_, "is", self.init_config[_])