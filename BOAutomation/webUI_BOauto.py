import model_BOauto
import view_BOauto
import controller_BOauto
import streamlit as st

class WebUI_BOauto:

    def __init__(self, model, view, controller):
        self.model = model
        self.view = view
        self.controller = controller
        self.view.ui_instance_set(self)
        # タイトル
        st.title("Optunaによる生産パラメータ探索アプリ")


    def run(self):
        self.controller.screen_manager(["initializer"])

    """以下、Viewからのコールバック"""
    def callback_syn_and_opr_check(self, custom_eval):
        self.controller.request_syntax_and_operation_check(custom_eval)

    def callback_save_init_config(self, init_config):
        self.controller.request_save_init_config(init_config)
        self.controller.screen_manager(["in_learning"])


model_opt = model_BOauto.Model_BOauto()
view_opt = view_BOauto.View_BOauto(model=model_opt)
controller_opt = controller_BOauto.Controller_BOauto(model=model_opt, view=view_opt)

ui = WebUI_BOauto(model_opt, view_opt, controller_opt)
ui.run()
